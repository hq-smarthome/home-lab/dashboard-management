# Dashboard Management

> :warning: **This project has been moved**
>
> Any further updates can be found here https://gitlab.com/carboncollins-cloud/dashboard-management
> This repository will be archived in favour of all further development at the new location.

Informational dashboards using [Grafana](https://grafana.com/)

## CI/CD

This repository loads its CI/CD pipeline from a common template found in the
[Job Template](https://gitlab.com/hq-smarthome/home-lab/job-template) repository

