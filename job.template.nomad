job "dashboard-management" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  vault {
    policies = ["job-dashboard-management"]
  }

  group "dashboards" {
    count = 1

    network {
      mode = "bridge"
    }

    service {
      name = "grafana"
      port = "3000"
      task = "grafana"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "loki"
              local_bind_port = 3100
            }

            upstreams {
              destination_name = "influxdb"
              local_bind_port = 8086
            }
          }
        }
      }

      check {
        expose = true
        name = "Application Health Status"
        type = "http"
        path = "/api/health"
        interval = "10s"
        timeout = "3s"
      }

      tags = [
        "traefik.enable=true",
        "traefik.consulcatalog.connect=true",
        "traefik.http.routers.grafana.entrypoints=https",
        "traefik.http.routers.grafana.tls=true",
        "traefik.http.routers.grafana.tls.certresolver=lets-encrypt",
        "traefik.http.routers.grafana.tls.domains[0].main=*.hq.carboncollins.se"
      ]
    }

    volume "grafana" {
      type = "host"
      source = "grafana"
      read_only = false
    }

    task "grafana" {
      driver = "docker"
      user = "1005"

      config {
        image = "[[ .grafanaImage ]]"
      }

      volume_mount {
        volume = "grafana"
        destination = "/var/lib/grafana"
        read_only = false
      }

      template {
        data = <<EOH
[[ fileContents "./config/config.template.env" ]]
        EOH

        destination = "secrets/config.env"
        env = true
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
    statefull = "true"
  }
}
